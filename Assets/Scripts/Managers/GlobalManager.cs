﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class GlobalManager : MonoBehaviour {

    public TextAsset[] textDocuments;
    public enum GameState {
        FreeMode,
        Pause,
        Started,
    }
    public static GameState currentState;
    bool calledTransition;
    static DataSave dataSave = new DataSave();

    private void Awake() {
        Transitioner.instance.CallFadeIn(Color.black, 1f);
        currentState = GameState.Started;
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Start, "Global Manager");
        calledTransition = true;
    }

    void Update() {
        if (Transitioner.waitingChanges && calledTransition) {
            if (currentState == GameState.Started) StartLoading();
        }

    }

    public void ChangeState (GameState newState) {

    }

    void StartLoading () {
        Load();
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Title, "Reading documents");
        for (int i = 0; i < textDocuments.Length; i++) {
            JSONtoOBJ.StartParser(textDocuments[i]);
        }
        GlobalArchive.SortLists();
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Title, "Reading documents finished");
        currentState = GameState.FreeMode;
        MenuManager.instance.StartMenuManager();
        Transitioner.instance.CallFadeOut();
        Save();
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Start, "Godai Assistant");
    }

    public static void Save(){
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GodaiAssistantSave.gd");
        bf.Serialize(file, dataSave);
        file.Close();
    }
    public static void Load() {
        if (File.Exists(Application.persistentDataPath + "/GodaiAssistantSave.gd")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GodaiAssistantSave.gd", FileMode.Open);
            dataSave = (DataSave)bf.Deserialize(file);
            dataSave.Load();
            file.Close();
        }
    }
}
