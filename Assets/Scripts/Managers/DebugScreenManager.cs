﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugScreenManager : MonoBehaviour {

    public static DebugScreenManager instance;

    public Text debugText;
    public RectTransform viewContent;
    public GameObject debugScreen;

    public enum MessageType {
        Normal,
        Search,
        Parser,
        Title,
        Start,
    }

    bool isDebugScreenActive;

    bool onHold;
    Queue<string> messageQueue = new Queue<string>();

    private void Awake() {
        instance = this;
        isDebugScreenActive = false;
        debugScreen.SetActive(isDebugScreenActive);
        CleanDebugScreen();
        WriteDebug(MessageType.Start, "Debug Screen Manager");
    }
    
    public void ShowDebugScreen () {
        debugScreen.SetActive(!isDebugScreenActive);
        isDebugScreenActive = debugScreen.activeSelf;
    }

    public void WriteDebug(MessageType type, string text) {
        string message = "";
        switch (type) {
            case MessageType.Normal:
                break;
            case MessageType.Parser:
                message += "<color=#00A708FF> Reading: </color>";
                break;
            case MessageType.Search:
                message += "<color=#003FD6FF> Searching: </color>";
                break;
            case MessageType.Start:
                message += "<color=#5C5C5CFF> Starting: </color>";
                break;
            case MessageType.Title:
                message += "<color=#FFAE00FF> Important: </color>";
                break;
        }
        message += text + "\n";
        if (debugText.text.Length >= 5900) {
            if (!onHold) {
                WriteError("Log is too long.\n Clean Log to receive more messages.");
                onHold = true;
            }
            messageQueue.Enqueue(message);
        }else {
            debugText.text += message;
        }
    }

    public void WriteError (string text) {
        debugText.text += "<color=red> " + text + "</color>\n";
    }

    public void CleanDebugScreen () {
        debugText.text = "\n";
        viewContent.sizeDelta = new Vector2();
        WriteMessagesOnQueue();
    }

    void WriteMessagesOnQueue () {
        while (messageQueue.Count > 0) {
            if (debugText.text.Length >= 5900) {
                WriteError("Log is too long. Clean Log to receive more messages.");
                return;
            }else {
                debugText.text += messageQueue.Peek();
                messageQueue.Dequeue();
            }
        }
        onHold = false;
    }
}
