﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenHolster : MenuHolster {

    public InformationHolster[] informationBoxes;
    int currentEntry;
    public Text searchBar;

    private void OnEnable () {
        Clean();
        currentEntry = 0;
        ChangeInformation();
    }

    void Clean () {
        for (int i = 0; i < informationBoxes.Length; i++) {
            informationBoxes[i].Clean();
        }
    }

    public void NextEntry (int i) {
        currentEntry += i;
        ChangeInformation();
    }

    void ChangeInformation() {
        List<string> temp;
        switch (screenID) {
            case "Abilities":
                if (currentEntry < 0)
                    currentEntry = GlobalArchive.sortedAbilityList.Count - 1;
                else if (currentEntry > GlobalArchive.sortedAbilityList.Count - 1)
                    currentEntry = 0;
                temp = GlobalArchive.sortedAbilityList[currentEntry].GetInformation();
                for (int i = 0; i < informationBoxes.Length; i++) {
                    informationBoxes[i].FillInformation(temp[i]);
                }
                ChangeButtons(GlobalArchive.sortedAbilityList.Count - 1);
                break;
        }
    }

    void ChangeButtons (int max) {
        if (currentEntry - 1 < 0) {
            texts[0].text = GetInformation(max);
        } else {
            texts[0].text = GetInformation(currentEntry - 1);
        }
        if (currentEntry + 1 > max) {
            texts[1].text = GetInformation(0);
        } else {
            texts[1].text = GetInformation(currentEntry + 1);
        }
    }

    string GetInformation (int i) {
        string t = "Error";
        switch (screenID) {
            case "Abilities":
                t = GlobalArchive.sortedAbilityList[i].GetName();
                break;
        }
        return t;
    }

    public void FindEntry() {
        int i = -1;
        switch (screenID) {
            case "Abilities":
                i = GlobalArchive.GetEntryID(searchBar.text, GlobalArchive.ListType.SortedAbility);
                break;
        }
        if (i > -1) {
            currentEntry = i;
            ChangeInformation();
        }
    }
}
