﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestingManager : MonoBehaviour {

    public string transitionType = "";
    public Color fadeColor;
    public float fadeSpeed;

    public GameObject[] currentObjects;
    public GameObject[] nextObjects;

    public bool calledFade;


    public void CallTransition () {
        switch (transitionType) {
            case "FadeIn":
                Transitioner.instance.CallFadeIn(fadeColor, fadeSpeed);
                calledFade = true;
                transitionType = "FadeOut";
                break;
            case "FadeOut":
                for (int i = 0; i < currentObjects.Length; i++) currentObjects[i].SetActive(false);
                for (int i = 0; i < nextObjects.Length; i++) nextObjects[i].SetActive(true);
                GameObject[] temp = currentObjects;
                currentObjects = nextObjects;
                nextObjects = temp;
                calledFade = false;
                transitionType = "";
                Transitioner.instance.CallFadeOut();
                break;
            case "Dissolve":
                break;
            default:
                break;
        }
    }

    void Update() {
        if (Transitioner.waitingChanges && calledFade) CallTransition();

    }

}
