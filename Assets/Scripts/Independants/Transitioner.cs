﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Transitioner : MonoBehaviour {

    enum TransitionType {
        FadeIn,
        FadeOut,
    }

    //Fade & Dissolve Variables
    GameObject blankImage;
    Color targetColor, currentColor;

    //General Variables
    GameObject[] currentObjects, nextObjects;
    float transitionSpeed, t;
    public static Transitioner instance;


    TransitionType transitionType;
    public static bool transitionIsActive;
    public static bool waitingChanges;

    void Awake() {
        instance = this;
        blankImage = transform.GetChild(0).gameObject;
        blankImage.SetActive(false);
        CleanTransitioner();
    }

    void CleanTransitioner () {
        blankImage.GetComponent<Image>().sprite = null;
        blankImage.GetComponent<Image>().color = new Color(0,0,0,0);
        blankImage.SetActive(false);
        targetColor = Color.white;
        currentColor = Color.white;
        currentObjects = new GameObject[1];
        nextObjects = new GameObject[1];
        transitionIsActive = false;
    }

    private void Update() {
        if (transitionIsActive) {
            switch (transitionType) {
                case TransitionType.FadeIn:
                    if (blankImage.GetComponent<Image>().color == targetColor){
                        transitionIsActive = false;
                        waitingChanges = true;
                    } else {
                        Fade();
                    }
                    break;
                case TransitionType.FadeOut:
                    if (blankImage.GetComponent<Image>().color == targetColor){
                        CleanTransitioner();
                        transitionIsActive = false;
                    } else {
                        Fade();
                    }
                    break;
            }
        }
    }

    public void CallFadeIn (Color fadeColor, float fadeSpeed) {
        targetColor = fadeColor;
        if (fadeSpeed > 0.0f)
            transitionSpeed = fadeSpeed;
        else
            transitionSpeed = 1f;
        currentColor = new Color(targetColor.r, targetColor.g, targetColor.b, 0.0f);
        blankImage.GetComponent<Image>().color = currentColor;
        transitionType = TransitionType.FadeIn;
        transitionIsActive = true;
        t = 0;
        blankImage.SetActive(true);
        Fade();
    }

    public void CallFadeOut () {
        transitionType = TransitionType.FadeOut;
        targetColor = currentColor;
        currentColor = blankImage.GetComponent<Image>().color;
        t = 0;
        waitingChanges = false;
        transitionIsActive = true;
        Fade();
    }

    public void CallDissolve (GameObject[] currentObjects_, GameObject[] nextObjects_, float dissolveSpeed) {
        currentObjects = currentObjects_;
        nextObjects = nextObjects_;
    }

    void Fade () {
        t += transitionSpeed * Time.deltaTime;
        if (t > 1) t = 1;
        Color c = Color.Lerp(currentColor, targetColor, t);
        blankImage.GetComponent<Image>().color = c;
    }

    void Disolve() {
        currentObjects[0].SetActive(false);
        nextObjects[0].SetActive(true);
    }
    
}
