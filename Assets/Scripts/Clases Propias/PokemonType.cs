﻿[System.Serializable]
public class PokemonType {

    int id;
    string name;
    string hexColor;

    public PokemonType () {
        id = 0;
        name = "";
        hexColor = "";
    }

    public PokemonType (int id_, string name_, string hexColor_) {
        id = id_;
        name = name_;
        hexColor = hexColor_;
    }

    //Fake Constructor for Parser
    public PokemonType (string id_, string name_, string hexColor_) {
        id = int.Parse(id_); ;
        name = name_;
        hexColor = hexColor_;
        if (GlobalArchive.EntryExists(id_, GlobalArchive.ListType.PokemonType))
            GlobalArchive.ReplaceByID(id, new PokemonType(id, name, hexColor));
        else
            GlobalArchive.pokemonTypeList.Add(new PokemonType(id, name, hexColor));
    }

    //Getters & Setters
    public int GetID () {
        return id;
    }
    public void SetID (int newID) {
        id = newID;
    }

    public string GetName () {
        return name;
    }
    public void SetName (string newName) {
        name = newName;
    }

    public string GetColor () {
        return hexColor;
    }
    public void SetColor (string newHexColor) {
        hexColor = newHexColor;
    }
}
