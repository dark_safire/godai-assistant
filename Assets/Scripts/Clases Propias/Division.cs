﻿[System.Serializable]
public class Division {

    int id;
    string name;
    int points;

    public Division () {
        id = 0;
        name = "";
        points = 0;
    }

    public Division (int id_, string name_, int points_) {
        id = id_;
        name = name_;
        points = points_;
    }

    //Fake Constructor for Parser
    public Division (string id_, string name_, string points_) {
        id = int.Parse(id_);
        name = name_;
        points = int.Parse(points_);
        if (GlobalArchive.EntryExists(id_, GlobalArchive.ListType.Division))
            GlobalArchive.ReplaceByID(id, new Division(id, name, points));
        else
            GlobalArchive.divisionList.Add(new Division(id, name, points));
    }

    //Getters & Setters

    public int GetID () {
        return id;
    }
    public void SetID (int newID) {
        id = newID;
    }

    public string GetName() {
        return name;
    }
    public void SetName (string newName) {
        name = newName;
    }

    public int GetPoints () {
        return points;
    }
    public void SetPoints (int newPoints) {
        points = newPoints;
    }
}
