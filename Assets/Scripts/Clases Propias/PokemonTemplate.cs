﻿public class PokemonTemplate {

    float id;
    string name;
    PokemonType type_1,type_2;
    Ability ability_1, ability_2;
    string height;
    string weight;
    HealthType health;
    ExpRate expRate;
    Division division;

    //Attack Limit Formula
    string aL;
    //Defense Limit Formula
    string dL;
    //Special Attack Limit Formula
    string spAL;
    //Special Defense Limit Formula
    string spDL;
    //Speed Limit Formula
    string sL;

    //Move Set ID, usually shared by evolution families
    string moveSetID;

    //Evolution Conditions
    string[] eC;
    //Evolution Forms
    PokemonTemplate[] eF;
    //Alternative Conditions
    string[] aC;
    //Alternative Forms
    PokemonTemplate[] aF;

    public PokemonTemplate () {
        id = 0.0f;
        name = "";
        type_1 = new PokemonType();
        type_2 = new PokemonType();
        ability_1 = new Ability();
        ability_2 = new Ability();
        height = "";
        weight = "";
        health = new HealthType();
        expRate = new ExpRate();
        division = new Division();
        aL = "";
        dL = "";
        spAL = "";
        spDL = "";
        sL = "";
        moveSetID = "";
        eC = new string[1];
        eF = new PokemonTemplate[1];
        aC = new string[1];
        aF = new PokemonTemplate[1];
    }

    public PokemonTemplate(float id_, string name_, PokemonType type_1_, PokemonType type_2_, Ability ability_1_, Ability ability_2_, string height_, 
        string weight_, HealthType health_, ExpRate expRate_, Division division_, string attackLimit, string defenseLimit, string spAttackLimit, 
        string spDefenseLimit, string speedLimit, string moveSetID_, string[] evolutionConditions, PokemonTemplate[] evolutionForms, string[] alternativeConditions, 
        PokemonTemplate[] alternativeForms) {
        id = id_;
        name = name_;
        type_1 = type_1_;
        type_2 = type_2_;
        ability_1 = ability_1_;
        ability_2 = ability_2_;
        height = height_;
        weight = weight_;
        health = health_;
        expRate = expRate_;
        division = division_;
        aL = attackLimit;
        dL = defenseLimit;
        spAL = spAttackLimit;
        spDL = spDefenseLimit;
        sL = speedLimit;
        moveSetID = moveSetID_;
        eC = evolutionConditions;
        eF = evolutionForms;
        aC = alternativeConditions;
        aF = alternativeForms;
    }

    //Getters & Setters
    public float GetID () {
        return id;
    }
    public void SetID (float newID) {
        id = newID;
    }

    public string GetName () {
        return name;
    }
    public void SetName (string newName) {
        name = newName;
    }

    public string GetType (int i) {
        string t = "Type Not Found";
        if (i == 1) t = type_1.GetColor() + type_1.GetName() + "</color>";
        if (i == 2) t = type_2.GetColor() + type_2.GetName() + "</color>";
        return t;
    }

     public void SetType (int i, PokemonType newType) {
        if (i == 1) type_1 = newType;
        if (i == 2) type_2 = newType;
    }

    public string GetAbility (int i) {
        string t = "Ability Not Found";
        if (i == 1) t = ability_1.GetName();
        if (i == 2) t = ability_2.GetName();
        return t;
    }

    public void SetAbility(int i, Ability newAbility) {
        if (i == 1) ability_1 = newAbility;
        if (i == 2) ability_2 = newAbility;
    }
}
