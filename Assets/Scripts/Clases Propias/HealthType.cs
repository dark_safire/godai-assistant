﻿[System.Serializable]
public class HealthType {

    int id;
    string name;
    string description;

    public HealthType () {
        id = 0;
        name = "";
        description = "";
    }

    public HealthType (int id_, string name_, string description_) {
        id = id_;
        name = name_;
        description = description_;
    }

    //Fake Constructor for Parser
    public HealthType (string id_, string name_, string description_) {
        id = int.Parse(id_); ;
        name = name_;
        description = description_;
        if (GlobalArchive.EntryExists(id_, GlobalArchive.ListType.HealthType))
            GlobalArchive.ReplaceByID(id, new HealthType(id, name, description));
        else
            GlobalArchive.healthTypeList.Add(new HealthType(id, name, description));
    }

    //Getters & Setters
    public int GetID () {
        return id;
    }
    public void SetID (int newID) {
        id = newID;
    }

    public string GetName () {
        return name;
    }
    public void SetName (string newName) {
        name = newName;
    }

    public string GetDescription () {
        return description;
    }
    public void SetDescription (string newDescription) {
        description = newDescription;
    }
}
