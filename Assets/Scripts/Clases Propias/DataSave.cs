﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataSave {

    float currentVersion;
    List<Ability> abilityList;
    List<Ability> sortedAbilityList;
    List<Division> divisionList;
    List<ExpRate> expRateList;
    List<HealthType> healthTypeList;
    List<PokemonType> pokemonTypeList;

    public DataSave () {
        currentVersion = GlobalArchive.currentVersion;
        abilityList = GlobalArchive.abilityList;
        sortedAbilityList = GlobalArchive.sortedAbilityList;
        divisionList = GlobalArchive.divisionList;
        expRateList = GlobalArchive.expRateList;
        healthTypeList = GlobalArchive.healthTypeList;
        pokemonTypeList = GlobalArchive.pokemonTypeList;
    }

    public void Load () {
        GlobalArchive.currentVersion = currentVersion;
        GlobalArchive.abilityList = abilityList;
        GlobalArchive.sortedAbilityList = sortedAbilityList;
        GlobalArchive.divisionList = divisionList;
        GlobalArchive.expRateList = expRateList;
        GlobalArchive.healthTypeList = healthTypeList;
        GlobalArchive.pokemonTypeList = pokemonTypeList;
    }

}
