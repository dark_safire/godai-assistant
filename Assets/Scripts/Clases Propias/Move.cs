﻿public class Move {

    string id;
    string name;
    PokemonType type;

    public enum Category {
        Physical,
        Special,
        Status,
    }

    public enum Contest {
        Tough,
        Cute,
        Clever,
        Beautiful,
        Cool,
        None,
    }

    Category category;
    Contest contest;
    int power;
    int accuracy;
    string description;

    public Move () {
        id = "";
        name = "";
        type = new PokemonType();
        category = Category.Physical;
        contest = Contest.None;
        power = 0;
        accuracy = 0;
        description = "";
    }

    public Move (string id_, string name_, PokemonType type_, Category category_, Contest contest_, int power_, int accuracy_, string description_) {
        id = id_;
        name = name_;
        type = type_;
        category = category_;
        contest = contest_;
        power = power_;
        accuracy = accuracy_;
        description = description_;
    }

    //Getters & Setters
    public string GetID () {
        return id;
    }
    public void SetID (string newID) {
        id = newID;
    }

    public string GetName () {
        return name;
    }
    public void SetName (string newName) {
        name = newName;
    }

    public PokemonType GetTypeID () {
        return type;
    }
    public void SetTypeID (PokemonType newType) {
        type = newType;
    }

    public Category GetCategory () {
        return category;
    }
    public void SetCategory (Category newCategory) {
        category = newCategory;
    }

    public Contest GetContest () {
        return contest;
    }

    public void SetContest(Contest newContest) {
        contest = newContest;
    }

    public int GetPower () {
        return power;
    }
    public void SetPower (int newPower) {
        power = newPower;
    }

    public int GetAccuracy () {
        return accuracy;
    }
    public void SetAccuracy (int newAccuracy) {
        accuracy = newAccuracy;
    }

    public string GetDescription() {
        return description;
    }
    public void SetDescription (string newDescription) {
        description = newDescription;
    }
}
