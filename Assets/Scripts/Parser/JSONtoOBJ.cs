﻿using UnityEngine;
using System;

public static class JSONtoOBJ {

    static string className;
    static bool upToDate;


    public static void StartParser(TextAsset json) {
        upToDate = false;
        className = "";
        PrepareText(json.text);
    }

    static void PrepareText(string text) {
        string newText = text.Replace("\r", string.Empty).Replace("\n", string.Empty);
        string[] strArr = newText.Split('}');
        for (int i = 0; i < strArr.Length - 1; i++) {
            string temp = strArr[i].Substring(1, strArr[i].Length - 1);
            if (i == 0) {
                className = temp;
            }else if (i == 1) {
                BuildTemplate(temp);
            } else {
                BuildOBJ(temp);
            }
            if (upToDate) return;
        }
        upToDate = false;
        className = "";
    }

    static void BuildOBJ(string stringOBJ) {
        Type objType = Type.GetType(className);
        string[] param = stringOBJ.Split(';');
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Normal, className + " with ID " + param[0]);
        Activator.CreateInstance(objType, param);
    }

    static void BuildTemplate (string stringOBJ) {
        string[] param = stringOBJ.Split(';');
        upToDate = GlobalArchive.CheckVersion(className, param[1]);
        if (upToDate) {
            DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Normal, className + " is Up To Date");
            return;
        }
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Parser, className + " Template");
        Type objType = Type.GetType(className);
        Activator.CreateInstance(objType, param);
    }

}